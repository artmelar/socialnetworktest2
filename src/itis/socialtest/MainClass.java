package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.*;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("", "");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        File postFile = new File(postsSourcePath);
        File authFile = new File(authorsSourcePath);

        try {
            FileReader fr = new FileReader(authFile);
            BufferedReader br = new BufferedReader(fr);

            List<Author> authors = new ArrayList<>();
            String string;
            while ((string = br.readLine()) != null) {
                Long id = Long.valueOf(string.substring(0, string.indexOf(',')));
                string = string.substring(string.indexOf(',') + 2);
                String nickname = string.substring(0, string.indexOf(','));
                String birthdayDate = string.substring(string.indexOf(',') + 2);

                authors.add(new Author(id, nickname, birthdayDate));
            }
            fr = new FileReader(postFile);
            br = new BufferedReader(fr);

            System.out.println(analyticsService.findPostsByDate(allPosts, "17.04.2021"));
            System.out.println(analyticsService.findMostPopularAuthorNickname(allPosts));
            System.out.println(analyticsService.checkPostsThatContainsSearchString(allPosts, "Россия"));
            System.out.println(analyticsService.findAllPostsByAuthorNickname(allPosts, "varlamov"));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
